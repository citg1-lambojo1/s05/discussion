<%-- 
	-imports in JSP can be achieved using directives.
	-Directives are defined by the "%@" symbol which is used to define the attribute of the page.
	- multiple imports can be achieved by adding a ",". (e.g. import = "java.util.Data, java.util.Scanner")
	 --%>
	 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import = "java.util.Date"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Java Server Page</title>
	</head>
	<body>
	<body>
		<h1>Welcome to Hotel Servlet</h1>
		<!-- JSP allows integration of HTML Tags with Java syntax -->
		<!-- JSP Scriplets -->
		<% System.out.println("Hello from JSP"); %>
		
		<!-- JSP Declaration -->
		<%!
			Date currentDateTime = new java.util.Date();
		%>
		
		<!-- JSP Expression -->
		<p> The time now is <%= currentDateTime %></p>
		
		<!-- 
		
			JSP Declaration (< %! % >)
				- Allows the declaration of one or more variables or methods.
				- Variables declared using the declaration tag are placed outside of __jspService Method().
		-->
		<%!
			private int initVar = 0;
			private int serviceVar = 0;
			private int destroyVar = 0;
		%>
		<!-- JSP method Declaration -->
		<%!
			public void jspInit(){
				initVar++;
				System.out.println("jspInit(): init" +initVar);
			}
			public void jspDestroy(){
				destroyVar++;
				System.out.println("jspDestroy(): destroy" +destroyVar);
			}
		%>
		
		<!-- 
			JSP Scriptlets (< %%>)
				- Allow any java language statement, variable, methods declarations or expressions.
				- Variables declared using scriplet tag is placed inside __jspService() method.
		 -->
		 <%
		 	serviceVar++;
		 	System.out.println("__jspService(): service" +serviceVar);
		 	
		 	String content1 = "content1 : " +initVar;
		 	String content2 = "content2 : " +serviceVar;
		 	String content3 = "content3 : " +destroyVar;
		 %>
		
		<!-- 
			JSP Expression (< %= %>)
				- code placed within the JSP expression tag is written to the output stream of the response
				- out.println is no longer required to print values of variables/methods.
		 -->
		 <h1>JSP Expression</h1>
		 <p><%= content1 %>
		 <p><%= content2 %>
		 <p><%= content3 %>
		 
		 <h1>Create an Account</h1>
		 <form action="user" method="post">
		 	<label for="firstname">First Name: </label><br>
		 	<input type="text" name="firstname"><br>
		 	<label for="lastname">Last Name: </label><br>
		 	<input type="text" name="lastname"><br>
		 	<label for="email">Email: </label><br>
		 	<input type="text" name="email"><br>
		 	<label for="contact">Contact No.: </label><br>
		 	<input type="text" name="contact"><br>
		 	
		 	<input type="submit">
		 </form>

			
	</body>
</html>